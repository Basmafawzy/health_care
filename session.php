<?php
include('conn.php');
session_start();

$user_check = $_SESSION['login_user'];

$stmt = $conn->prepare("SELECT username, role FROM admin WHERE username = ?");
$stmt->bind_param("s", $user_check);
$stmt->execute();

$result = $stmt->get_result();

// Check if the user exists
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $login_session = $row['username'];
    $role_session = $row['role'];
} else {
    header("location:/Health/login.php");
    exit();
}

$stmt->close(); 

?>
