<?php
include '../conn.php';

$ticketId = mysqli_real_escape_string($conn, $_GET['ticketId']);

$query = "SELECT t.id, t.closed, s.stdname AS student_name, c.name AS category_name, t.date, t.note,
                 f.name AS faculty_name, l.name AS level_name
          FROM ticket t
          JOIN student s ON t.student_id = s.id
          JOIN category c ON t.category_id = c.id
          JOIN faculty f ON s.faculty_id = f.id
          JOIN level l ON s.level_id = l.id
          WHERE t.id = '$ticketId'";

$result = mysqli_query($conn, $query);

$ticket = mysqli_fetch_assoc($result);

mysqli_close($conn);

// Include TCPDF library
require_once('../tcpdf/tcpdf.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetTitle('التأمين الصحى لجامعة المنوفية الأهلية');




$lg = Array();
$lg['a_meta_charset'] = 'UTF-8';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';

$pdf->setLanguageArray($lg);
$pdf->SetFont('aealarabiya', '', 15);

$pdf->AddPage();



$datl = <<<EOD
<div >
<div style="color: #000; font-size: 17px; line-height: 1.5;">
<div >جامعة المنوفية الأهلية <br />
نظام التأمين الصحى </div>
</div>


EOD;


$pdf->writeHTML($datl, true, false, false, false, '');

$pdf->Image('../images\logo_bill.png', 135, 15, 40, 40, 'PNG',  true);

$pdf->SetXY(70, 35);




$pdf->SetXY(60,2);
$pdf->Cell(0, 0, 'تصميم وحدة الخدمات الإلكترونية جامعة المنوفية الأهلية ' , 0, 1);


$pdf->SetXY(140, 25);
$pdf->Cell(0,10,  '    رقم التذكرة: '.  $ticket['id'] , 0, 1);
$pdf->SetXY(145, 35);
$pdf->Cell(0, 10, 'التاريخ: ' . date('d/m/Y', strtotime($ticket['date'])), 0, 1);
$pdf->SetXY(140, 45);
$pdf->Cell(0, 10,  '  الجهة :'. $ticket['category_name'], 0, 1);
$pdf -> Line(200, 60, 10, 60);

$pdf->SetXY(15, 60);
$pdf->Cell(0,10,  ' إسم الطالب : '. $ticket['student_name'] , 0, 1);
$pdf->SetXY(17, 70);
$pdf->Cell(0, 10, 'الكلية: '. $ticket['faculty_name'], 0, 1);
$pdf->SetXY(15, 80);
$pdf->Cell(0, 10,  ' التشخيص  :', 0, 1);

$pdf->SetXY(130, 60);
$pdf->Cell(0,10,  '     النوع : ( ذكر - انثى ) السن: ' , 0, 1);
$pdf->SetXY(132, 70);
$pdf->Cell(0, 10, '   الفرقة:'. $ticket['level_name'], 0, 1);






$tbl = <<<EOD
<h2 align="center">الأبحاث المطلوبة - العلاج المقرر</h2>

 <table cellspacing="0" cellpadding="1" border="1" style="border-color:gray;">

 <tr style="background-color:green;color:white;text-align:center;font-size:15px;padding: top 10px;">
     <td style="height: 30px;width: 190px">العلاج </td>
     <td style="width: 40px;">الوحدة</td>
     <td style="width: 40px;">الجرعة</td>
     <td style="width: 50px;">الكمية المنصرفة </td>
     <td style="width: 60px;">سعرالوحدة  </td>
    
     
     <td style="width: 70px">الإجمالي</td>
     <td style="width: 90px;">ملاحظات </td>
 </tr>



 <tr>
     <td style="height: 360px;"></td>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
    
 </tr>
</table>


EOD;
$pdf->SetY( 90);
$pdf->writeHTML($tbl, true, false, false, false, '');

$pdf->SetXY(20,245);

$pdf->Cell(0, 10,'فقط وقدره  ', 0, 1);
$pdf->SetXY(110,245);

$pdf->Cell(0, 10,'القيمة الإجمالية ', 0, 1);

$pdf->SetXY(25,260);

$pdf->Cell(0, 10,'الطبيب  ', 0, 1);

$pdf->SetXY(130,260);
$pdf->Cell(0, 10, 'الصيدلي ' , 0, 1);



$pdf->setRTL(true);

ob_end_clean();
$pdf->Output('ticket_details.pdf', 'I');
