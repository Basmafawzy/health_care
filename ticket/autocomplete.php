<?php
include '../conn.php';

$searchTerm = mysqli_real_escape_string($conn, $_GET['term']);

$query = "SELECT DISTINCT s.stdname, s.nationalnum
          FROM student s
          JOIN year y ON s.year_id = y.id
          WHERE (s.stdname LIKE '%$searchTerm%' OR s.nationalnum LIKE '%$searchTerm%')
          AND y.active = 1
          LIMIT 10"; 


$result = mysqli_query($conn, $query);

$autocompleteResults = array();

while ($row = mysqli_fetch_assoc($result)) {
    $autocompleteResults[] = $row['stdname'];
}

echo json_encode($autocompleteResults);

mysqli_close($conn);
?>
