<?php
include_once '../conn.php';
include('../header.php');
$recordsPerPage = isset($_GET['perPage']) ? $_GET['perPage'] : 10;

$currentPage = isset($_GET['page']) ? $_GET['page'] : 1;

$offset = ($currentPage - 1) * $recordsPerPage;

$query = "SELECT s.id,s.stdname, s.nationalnum, f.name AS faculty_name, l.name AS level_name, s.current_cost, s.total_cost
          FROM student s
          JOIN faculty f ON s.faculty_id = f.id
          JOIN level l ON s.level_id = l.id
          JOIN year y ON s.year_id = y.id
          WHERE y.active = 1
          LIMIT $offset, $recordsPerPage";

$result = mysqli_query($conn, $query);
?>
<!DOCTYPE html>
<html lang="ar" dir="rtl" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>نظام التأمين الصحي -  فتح تذكرة جديدة</title>
    <!-- Include Bootstrap styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/fontawesome.css">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/owl.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet"href="../css/swiper-bundle.min.css"/>
    <script src="../js/jquery.min.js"></script>
     <script src="../js/bootstrap.min.js"></script>
    <script src="../js/isotope.min.js"></script>
    <script src="../js/owl-carousel.js"></script>
     <script src="../js/counter.js"></script>
     <script src="../js/custom.js"></script>
     <script src="../js/jquery-ui.js"></script>

      <style>

body {
    background-color: #f4f4f4;
    font-family: 'Cairo', sans-serif;

}

#file {
        border: 1px solid #ced4da; 
        border-radius: 0.25rem;
        padding: 0.375rem 0.75rem; 
    }
    #autocomplete-container {
        width: 100%;
        background-color: #fff;
        max-height: 200px;
        overflow-y: auto;
        z-index: 1000;
    }

    .ui-autocomplete-category {
        font-weight: bold;
        padding: 8px;
        background-color: #f5f5f5;
    }

    .ui-menu-item {
        padding: 8px;
        cursor: pointer;
    }

    .ui-menu-item:hover {
        background-color: #f0f0f0;
    }
</style>

<script>
        $(document).ready(function () {

            if (performance.navigation.type == 1) {
                $("#searchInput").val('');
            }
            
            $("#searchInput").autocomplete({
            source: "/Health/ticket/autocomplete.php",
            minLength: 4, 
            select: function (event, ui) {
                searchStudents(ui.item.value);
            },
            appendTo: "#autocomplete-container", 
        });
            $("#searchButton").on("click", function () {
                var searchTerm = $("#searchInput").val();
                searchStudents(searchTerm);
            });

            function searchStudents(searchTerm) {
                $.ajax({
                    url: "/Health/ticket/search.php", 
                    type: "POST",
                    data: { searchTerm: searchTerm },
                    success: function (data) {
                        $("#studentTableBody").html(data);
                    }
                });
            }
        });
    </script>

</head>
<body>
    <?php  ?>
<section style="margin: 10px 0;">
    <div class="container mt-5">
  <h2>فتح تذكرة جديدة</h2>
  <br/>


<div class="mb-3">
            <label for="searchInput" class="form-label">البحث بالإسم او الرقم القومي</label>
            <input type="text" class="form-control"  name="searchInput" id="searchInput" placeholder="البحث بالإسم او الرقم القومي">
            <div id="autocomplete-container"></div>
        </div>
        <div class="mb-3">
            <button type="button" class="btn btn-primary" id="searchButton" name="searchButton">ابحث</button>
            </div>   
            <form method="GET" action="">
                <label for="perPage">عدد السجلات في الصفحة:</label>
                <select name="perPage" id="perPage" onchange="this.form.submit()">
                    <option value="10" <?php echo ($recordsPerPage == 10) ? 'selected' : ''; ?>>10</option>
                    <option value="20" <?php echo ($recordsPerPage == 20) ? 'selected' : ''; ?>>20</option>
                    <!-- Add more options as needed -->
                </select>
            </form>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>الإسم</th>
                        <th>الرقم القومي</th>
                        <th>الكلية</th>
                        <th>الفرقة</th>
                        <th>المبلغ المستخدم</th>
                        <th>المبلغ المتاح</th>
                        <th>تذكرة جديدة</th>
                    </tr>
                </thead>
                <tbody id="studentTableBody" name="studentTableBody">
                    <?php
                    while ($row = mysqli_fetch_assoc($result)) {
                        echo "<tr>";
                        echo "<td>{$row['stdname']}</td>";
                        echo "<td>{$row['nationalnum']}</td>";
                        echo "<td>{$row['faculty_name']}</td>";
                        echo "<td>{$row['level_name']}</td>";
                        echo "<td>{$row['current_cost']}</td>";
                        echo "<td>{$row['total_cost']}</td>";
                        echo "<td><a class='btn btn-primary' href='create_ticket.php?student_id={$row['id']}'>فتح تذكرة</a></td>";
                         echo "</tr>";
                    }
                    $query = "SELECT COUNT(*) AS total FROM student WHERE year_id IN (SELECT id FROM year WHERE active = 1)";
                    $result = mysqli_query($conn, $query);
                    $row = mysqli_fetch_assoc($result);
                    $totalPages = ceil($row['total'] / $recordsPerPage);
                    mysqli_close($conn);
                    ?>
                </tbody>
            </table>
            

            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <?php
                    for ($i = 1; $i <= $totalPages; $i++) {
                        echo "<li class='page-item " . (($currentPage == $i) ? 'active' : '') . "'>";
                        echo "<a class='page-link' href='?page=$i&perPage=$recordsPerPage'>$i</a>";
                        echo "</li>";
                    }
                    ?>
                </ul>
            </nav>
        </div>
    </section>

    <?php include('../footer.php'); ?>