<?php
// Include database connection file
include '../conn.php';

$searchTerm = mysqli_real_escape_string($conn, $_POST['searchTerm']);

$query = "SELECT s.id, s.stdname, s.nationalnum, f.name AS faculty_name, l.name AS level_name, s.current_cost, s.total_cost
          FROM student s
          JOIN faculty f ON s.faculty_id = f.id
          JOIN level l ON s.level_id = l.id
          JOIN year y ON s.year_id = y.id
          WHERE (s.stdname LIKE '%$searchTerm%' OR s.nationalnum LIKE '%$searchTerm%')
          AND y.active = 1";


$result = mysqli_query($conn, $query);

$html = '';

while ($row = mysqli_fetch_assoc($result)) {
  $html .= "<tr>";
  $html .= "<td>{$row['stdname']}</td>";
  $html .= "<td>{$row['nationalnum']}</td>";
  $html .= "<td>{$row['faculty_name']}</td>";
  $html .= "<td>{$row['level_name']}</td>";
  $html .= "<td>{$row['current_cost']}</td>";
  $html .= "<td>{$row['total_cost']}</td>";
  $html .= "<td><a class='btn btn-primary' href='create_ticket.php?student_id={$row['id']}'>فتح تذكرة</a></td>";

  $html .= "</tr>";
}

echo $html;

mysqli_close($conn);
?>
