<?php
include '../conn.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $student_id = $_POST['student_id'];
    $category_id = $_POST['category_id'];
    $date = date('Y-m-d H:i:s'); 
    $ticket_notes = $_POST['ticket_notes'];

    $checkOpenQuery = "SELECT COUNT(*) AS openTickets FROM ticket WHERE student_id = '$student_id' AND closed = 0";
    $checkOpenResult = mysqli_query($conn, $checkOpenQuery);

    if ($checkOpenResult) {
        $openTickets = mysqli_fetch_assoc($checkOpenResult)['openTickets'];

        if ($openTickets > 0) {
            $lastTicketId = mysqli_insert_id($conn);
            echo json_encode(['success' => false, 'message' => 'يوجد تذكرة مفتوحة للطالب من فضلك قم بإغلاقها قبل فتح تذكرة جديدة']);
           

            exit();
        }
    } else {
        echo json_encode(['success' => false, 'message' => 'حدث خطأ في العملية']);
        exit();
    }

    $insertQuery = "INSERT INTO ticket (student_id, category_id, date,note) VALUES ('$student_id', '$category_id', '$date','$ticket_notes')";
    $result = mysqli_query($conn, $insertQuery);

    if ($result) {
        // Retrieve the last inserted ticket ID
        $lastTicketId = mysqli_insert_id($conn);

      

    // Now, send the response
    echo json_encode(['success' => true, 'message' => 'تم إضافة التذكرة بنجاح', 'lastTicketId' => $lastTicketId]);
        exit();
    } else {
        echo json_encode(['success' => false, 'message' => 'خطأ في اضافة التذكرة.']);
        exit();
    }
} else {
    echo json_encode(['success' => false, 'message' => 'عملية خاطئة.']);
    exit();
}

mysqli_close($conn);
?>
