<?php
include '../conn.php';

$ticketId = mysqli_real_escape_string($conn, $_GET['ticketId']);

$query = "SELECT t.id, t.closed, s.stdname AS student_name, c.name AS category_name, t.date, t.note,
                 f.name AS faculty_name, l.name AS level_name
          FROM ticket t
          JOIN student s ON t.student_id = s.id
          JOIN category c ON t.category_id = c.id
          JOIN faculty f ON s.faculty_id = f.id
          JOIN level l ON s.level_id = l.id
          WHERE t.id = '$ticketId'";

$result = mysqli_query($conn, $query);

$ticket = mysqli_fetch_assoc($result);

mysqli_close($conn);

// Include TCPDF library
require_once('../tcpdf/tcpdf.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetTitle('التأمين الصحى لجامعة المنوفية الأهلية');




$lg = Array();
$lg['a_meta_charset'] = 'UTF-8';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';

$pdf->setLanguageArray($lg);
$pdf->SetFont('aealarabiya', '', 15);

$pdf->AddPage();



$datl = <<<EOD
<div >
<div style="color: #000; font-size: 17px; line-height: 1.5;">
<div >جامعة المنوفية الأهلية <br />
نظام التأمين الصحى </div>
</div>


EOD;

$pdf->SetXY(50,10);
$pdf->writeHTML($datl, true, false, false, false, '');

$pdf->Image('../images\logo_bill.png', 130, 15, 40, 40, 'PNG',  true);

$pdf->SetXY(70, 35);

$pdf->SetXY(60,2);
$pdf->Cell(0, 0, 'تصميم وحدة الخدمات الإلكترونية جامعة المنوفية الأهلية ' , 0, 1);
$pdf->SetXY(140, 25);
$pdf->Cell(0,10,  ' رقم التذكرة: '.  $ticket['id'] , 0, 1);
$pdf->SetXY(145, 35);
$pdf->Cell(0, 10, 'التاريخ: ' . date('d/m/Y', strtotime($ticket['date'])), 0, 1);
$pdf->SetXY(140, 45);
$pdf->Cell(0, 10,  '  الجهة :'. $ticket['category_name'], 0, 1);
$pdf -> Line(200, 60, 10, 60);


$pdf->SetXY(90, 60);
$pdf->Cell(0,10,  'تقــرير طبــي', 0, 1);




$pdf->SetXY(15, 70);
$pdf->Cell(0,10,  ' إسم الطالب : '. $ticket['student_name'] , 0, 1);
$pdf->SetXY(17, 83);
$pdf->Cell(0, 10, 'الكلية: '. $ticket['faculty_name'], 0, 1);
$pdf->SetXY(15, 96);
$pdf->Cell(0, 10,  ' جهة طلب التقرير   :', 0, 1);

$pdf->SetXY(15, 108);
$pdf->Cell(0, 10,  ' التشخيص :', 0, 1);

$pdf->SetXY(130, 70);
$pdf->Cell(0,10,  '     النوع : ( ذكر - انثى ) السن: ' , 0, 1);
$pdf->SetXY(133, 83);
$pdf->Cell(0, 10, '   الفرقة:'. $ticket['level_name'], 0, 1);
$pdf->SetXY(133, 90);

$pdf -> Line(170, 113, 10, 113);
$pdf -> Line(190, 128, 10, 128);
$pdf -> Line(190, 148, 10, 148);
$pdf->SetXY(15, 165);
$pdf->Cell(0, 10,  ' التوصية الطبية :', 0, 1);
$pdf -> Line(160, 170, 10, 170);
$pdf -> Line(190, 185, 10, 185);
$pdf -> Line(190, 205, 10, 205);









$pdf->SetXY(35,220);

$pdf->Cell(0, 10,'توقيع الطبيب    ', 0, 1);

$pdf->SetXY(140,220);
$pdf->Cell(0, 10, 'يعتمد' , 0, 1);


$pdf -> Line(200, 260, 10, 260);

$pdf->SetXY(20,263);

$pdf->Cell(0, 10,'طريق القاهرة الأسكندرية الزراعي - الكيلو 70    ', 0, 1);



$pdf->SetXY(110,263);

$pdf->Cell(0, 10,'https://mnu.menofia.education   ', 0, 1);





$pdf->setRTL(true);

ob_end_clean();
$pdf->Output('medical report_pdf.pdf', 'I');
