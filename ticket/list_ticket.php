<?php
include '../conn.php';
include('../header.php');
// Specify the number of records to display per page
$recordsPerPage = isset($_GET['perPage']) ? $_GET['perPage'] : 10;

// Get the current page from the URL, default to 1 if not set
$currentPage = isset($_GET['page']) ? $_GET['page'] : 1;

// Calculate the offset based on the current page and records per page
$offset = ($currentPage - 1) * $recordsPerPage;

// Define an empty search term
$searchTerm = '';

// Check if a search term is provided
if (isset($_GET['searchInput'])) {
    $searchTerm = mysqli_real_escape_string($conn, $_GET['searchInput']);
}

$query = "SELECT t.id, t.closed, s.stdname AS student_name, c.name AS category_name, t.date, t.note As ticket_notes
          FROM ticket t
          JOIN student s ON t.student_id = s.id
          JOIN category c ON t.category_id = c.id ORDER BY t.id DESC" ;

// Add a search condition if a search term is provided
if (!empty($searchTerm)) {
    $query .= " WHERE t.id LIKE '%$searchTerm%' OR s.stdname LIKE '%$searchTerm%' OR c.name LIKE '%$searchTerm%'";
}

$query .= " LIMIT $offset, $recordsPerPage";

$result = mysqli_query($conn, $query);

if (!$result) {
    // If there is an error in the query, display the error message and exit
    die('Error in SQL query: ' . mysqli_error($conn));
}

?>
<!DOCTYPE html>
<html lang="ar" dir="rtl" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>نظام التأمين الصحي -  بحث عن تذكرة</title>
    <!-- Include Bootstrap styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/fontawesome.css">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/owl.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet"href="../css/swiper-bundle.min.css"/>
    <link rel="stylesheet"href="../css/jquery.dataTables.css"/>
    <script src="../js/jquery.min.js"></script>
     <script src="../js/bootstrap.min.js"></script>
    <script src="../js/isotope.min.js"></script>
    <script src="../js/owl-carousel.js"></script>
     <script src="../js/counter.js"></script>
     <script src="../js/custom.js"></script>
     <script src="../js/jquery-ui.js"></script>
     <script src="../js/jquery.dataTables.js"></script>
      <style>

<style>
        body {
            background-color: #f4f4f4;
            font-family: 'Cairo', sans-serif;
        }

        .custom-table {
            width: 100%;
            margin: auto;
            font-size: 18px;
            border: 2px solid #dddddd;
            border-radius: 10px;
            overflow: hidden;
        }

        .custom-table th,
        .custom-table td {
            border-bottom: 1px solid #dddddd;
            padding: 10px;
        }
    </style>
<script>
   $(document).ready(function() {
        $('#yourTableId').DataTable({
            "order": [] 
        });
    });
    function closeTicket(ticketId) {
        $.ajax({
            url: 'close_ticket.php',
            type: 'POST',
            data: { ticketId: ticketId },
            success: function(response) {
                location.reload();
            },
            error: function() {
                alert('حدث مشكلة عند غلق التذكرة');
            }
        });
    }

    function openTicket(ticketId) {
        
        $.ajax({
            url: 'open_ticket.php', 
            type: 'POST',
            data: { ticketId: ticketId },
            success: function(response) {
                location.reload();
            },
            error: function() {
                alert('حدث مشكلة عند فتح التذكرة.');
            }
        });
    }

$(document).ready(function () {
    $("#searchButton").on("click", function () {
        var searchTerm = $("#searchInput").val();

        $.ajax({
            url: "search_ticket.php", 
            type: "GET",
            data: { searchTerm: searchTerm },
            success: function (data) {
                $("#ticketTableBody").html(data);
            },
            error: function () {
                alert("Error occurred while processing the search.");
            }
        });
    });
});
</script>
</head>
<body>
    <?php  ?>
 

            <section style="margin: 10px 0;">
        <div class="container mt-5">
            <h2>بحث عن تذكرة</h2>
            <br />

            <div class="mb-3">
                <label for="searchInput" class="form-label">بحث</label>
                <input type="text" class="form-control" name="searchInput" id="searchInput" placeholder="البحث">
                <div id="autocomplete-container"></div>
            </div>
            <div class="mb-3">
                <button type="button" class="btn btn-primary" id="searchButton" name="searchButton">ابحث</button>
            </div>

            <form method="GET" action="">
                <label for="perPage">عدد السجلات في الصفحة:</label>
                <select name="perPage" id="perPage" onchange="this.form.submit()">
                    <option value="10" <?php echo ($recordsPerPage == 10) ? 'selected' : ''; ?>>10</option>
                    <option value="20" <?php echo ($recordsPerPage == 20) ? 'selected' : ''; ?>>20</option>
                    <!-- Add more options as needed -->
                </select>
            </form>

            <table class="table table-striped dataTable" >
                <thead>
                    <tr>
                        <th class="sortable">رقم التذكرة</th>
                        <th class="sortable">الحالة</th>
                        <th class="sortable">اسم الطالب</th>
                        <th class="sortable">الجهة</th>
                        <th class="sortable">ملحوظات</th>
                        <th class="sortable">تاريخ الإنشاء</th>
                        <th class="sortable">العملية</th>
                         <th class="sortable">الطباعة</th>
                         <th class="sortable"> تحويل</th>
                         <th class="sortable">تقرير طبي</th>
                    </tr>
                </thead>
                <tbody id="ticketTableBody" name="ticketTableBody">
                    <?php
                    while ($row = mysqli_fetch_assoc($result)) {
                        echo "<tr>";
                        echo "<td>{$row['id']}</td>";
                        echo "<td>" . ($row['closed'] == 0 ? 'مفتوحة' : 'مغلقة') . "</td>";
                        echo "<td>{$row['student_name']}</td>"; 
                        echo "<td>{$row['category_name']}</td>"; 
                        echo "<td>{$row['ticket_notes']}</td>"; 
                        echo "<td>" . date('d-m-Y', strtotime($row['date'])) . "</td>";
                        if ($row['closed'] == 0) {
                            echo "<td><button class='btn btn-sm btn-danger' onclick='closeTicket({$row['id']})'>اغلاق تذكرة</button></td>"; 
                        } else {
                            echo "<td><button class='btn btn-sm btn-success' onclick='openTicket({$row['id']})'>فتح تذكرة </button></td>"; 
                        }
                    
                        echo "<td>
                            <form method='GET' action='generate_pdf.php' target='_blank'>
                                <input type='hidden' name='ticketId' value='{$row['id']}'>
                                <button type='submit' class='btn btn-sm btn-primary'>طباعة </button>
                            </form>
                        </td>";
                    
                        echo "<td>
                            <form method='GET' action='ehtar-pdf.php' target='_blank'>
                                <input type='hidden' name='ticketId' value='{$row['id']}'>
                                <button type='submit' class='btn btn-sm btn-warning'>إخطار تحويل </button>
                            </form>
                        </td>";
                    
                        echo "<td>
                            <form method='GET' action='medical report_pdf.php' target='_blank'>
                                <input type='hidden' name='ticketId' value='{$row['id']}'>
                                <button type='submit' class='btn btn-sm btn-danger'>تقرير طبي </button>
                            </form>
                        </td>";
                   
    
                echo "</tr>";
           
                        echo "</tr>";
                    }
                    // Calculate the total number of pages
                    $query = "SELECT COUNT(*) AS total FROM ticket";
                    $result = mysqli_query($conn, $query);
                    $row = mysqli_fetch_assoc($result);
                    $totalPages = ceil($row['total'] / $recordsPerPage);
                    // Close database connection
                    mysqli_close($conn);
                    ?>
                </tbody>
            </table>

            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <?php
                    // Display page numbers
                    for ($i = 1; $i <= $totalPages; $i++) {
                        echo "<li class='page-item " . (($currentPage == $i) ? 'active' : '') . "'>";
                        echo "<a class='page-link' href='?page=$i&perPage=$recordsPerPage'>$i</a>";
                        echo "</li>";
                    }
                    ?>
                </ul>
            </nav>
        </div>
    </section>

<?php include('../footer.php'); ?>