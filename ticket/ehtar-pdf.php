<?php
include '../conn.php';

$ticketId = mysqli_real_escape_string($conn, $_GET['ticketId']);

$query = "SELECT t.id, t.closed, s.stdname AS student_name, c.name AS category_name, t.date, t.note,
                 f.name AS faculty_name, l.name AS level_name
          FROM ticket t
          JOIN student s ON t.student_id = s.id
          JOIN category c ON t.category_id = c.id
          JOIN faculty f ON s.faculty_id = f.id
          JOIN level l ON s.level_id = l.id
          WHERE t.id = '$ticketId'";

$result = mysqli_query($conn, $query);

$ticket = mysqli_fetch_assoc($result);

mysqli_close($conn);

// Include TCPDF library
require_once('../tcpdf/tcpdf.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetTitle('التأمين الصحى لجامعة المنوفية الأهلية');




$lg = Array();
$lg['a_meta_charset'] = 'UTF-8';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';

$pdf->setLanguageArray($lg);
$pdf->SetFont('aealarabiya', '', 15);

$pdf->AddPage();



$datl = <<<EOD
<div >
<div style="color: #000; font-size: 17px; line-height: 1.5;">
<div >جامعة المنوفية الأهلية <br />
نظام التأمين الصحى </div>
</div>


EOD;


$pdf->writeHTML($datl, true, false, false, false, '');

$pdf->Image('../images\logo_bill.png', 135, 15, 40, 40, 'PNG',  true);

$pdf->SetXY(70, 35);



$pdf->SetXY(60,2);
$pdf->Cell(0, 0, 'تصميم وحدة الخدمات الإلكترونية جامعة المنوفية الأهلية ' , 0, 1);


$pdf->SetXY(140, 25);
// $pdf->Cell(0,10,  '    رقم التذكرة: '.  $ticket['id'] , 0, 1);
$pdf->SetXY(145, 28);
$pdf->Cell(0, 10, 'التاريخ: ' . date('d/m/Y', strtotime($ticket['date'])), 0, 1);
$pdf->SetXY(140, 41);
$pdf->Cell(0, 10,  '  الجهة :'. $ticket['category_name'], 0, 1);
$pdf -> Line(200, 60, 10, 60);


$pdf->SetXY(90, 60);
$pdf->Cell(0,10,  'إخطار تحويل مريض  ', 0, 1);




$pdf->SetXY(15, 70);
$pdf->Cell(0,10,  ' إسم الطالب : '. $ticket['student_name'] , 0, 1);
$pdf->SetXY(17, 80);
$pdf->Cell(0, 10, 'الكلية: '. $ticket['faculty_name'], 0, 1);
$pdf->SetXY(15, 90);
$pdf->Cell(0, 10,  ' التشخيص  :', 0, 1);

$pdf->SetXY(15, 100);
$pdf->Cell(0, 10,  ' سبب التحويل   :', 0, 1);

$pdf->SetXY(130, 70);
$pdf->Cell(0,10,  '     النوع : ( ذكر - انثى ) السن: ' , 0, 1);
$pdf->SetXY(133, 80);
$pdf->Cell(0, 10, '   الفرقة:'. $ticket['level_name'], 0, 1);
$pdf->SetXY(133, 90);
$pdf->Cell(0, 10,  '   الجهه المحول إليها:', 0, 1);
$pdf -> Line(160, 106, 10, 106);
$pdf -> Line(200, 125, 10, 125);
$pdf -> Line(200, 145, 10, 145);
$pdf -> Line(200, 165, 10, 165);
$pdf -> Line(200, 185, 10, 185);


$pdf->SetXY(90,195);

$pdf->Cell(0, 10,'إمضاء الاخصائي : ', 0, 1);

$pdf->SetXY(15, 215);
$pdf->Cell(0, 10,  'السيد الدكتور :', 0, 1);


$pdf -> Line(160, 221, 10, 221);

$pdf->SetXY(70, 230);
$pdf->Cell(0, 10,  'برجاء إفادتنا عما تم بشأن الطالب المحول ،،،', 0, 1);



$pdf->SetXY(25,255);

$pdf->Cell(0, 0,'مدير العيادات   ', 0, 1);

$pdf->SetXY(130,255);
$pdf->Cell(0, 0, 'مدير المستشفى ' , 0, 1);



$pdf->setRTL(true);

ob_end_clean();
$pdf->Output('ehtar-pdf.pdf', 'I');
