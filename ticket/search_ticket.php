<?php
include '../conn.php';

$searchTerm = mysqli_real_escape_string($conn, $_GET['searchTerm']);

$query = "SELECT t.id, t.closed, s.stdname AS student_name, c.name AS category_name, t.date, t.note As ticket_notes
          FROM ticket t
          JOIN student s ON t.student_id = s.id
          JOIN category c ON t.category_id = c.id
          WHERE (t.id LIKE '%$searchTerm%' OR s.stdname LIKE '%$searchTerm%' OR c.name LIKE '%$searchTerm%') ORDER BY t.id DESC";

$result = mysqli_query($conn, $query);

$html = '';

while ($row = mysqli_fetch_assoc($result)) {
    $html .= "<tr>";
    $html .= "<td>{$row['id']}</td>";
    $html .= "<td>" . ($row['closed'] == 0 ? 'مفتوحة' : 'مغلقة') . "</td>";
    $html .= "<td>{$row['student_name']}</td>";
    $html .= "<td>{$row['category_name']}</td>";
    $html .= "<td>{$row['ticket_notes']}</td>";
    $html .= "<td>" . date('d-m-Y', strtotime($row['date'])) . "</td>";
    
    if ($row['closed'] == 0) {
        $html .= "<td><button class='btn btn-danger' onclick='closeTicket({$row['id']})'>اغلاق تذكرة </button></td>";
    } else {
        $html .= "<td><button class='btn btn-success' onclick='openTicket({$row['id']})'>فتح تذكرة </button></td>";
    }
    $html .= "<td>
    <form method='GET' action='generate_pdf.php' target='_blank'>
        <input type='hidden' name='ticketId' value='{$row['id']}'>
        <button type='submit' class='btn btn-primary'>طباعة </button>
    </form>
 </td>";

 $html .="<td>
 <form method='GET' action='ehtar-pdf.php' target='_blank'>
     <input type='hidden' name='ticketId' value='{$row['id']}'>
     <button type='submit' class='btn btn-warning'>إخطار تحويل </button>
 </form>
</td>";

$html .="<td>
 <form method='GET' action='ehtar-pdf.php' target='_blank'>
     <input type='hidden' name='ticketId' value='{$row['id']}'>
     <button type='submit' class='btn btn-warning'>تقرير طبي</button>
 </form>
</td>";
 

    $html .= "</tr>";
    $html .= "</tr>";
    $html .= "</tr>";
}

echo $html;

mysqli_close($conn);
?>
