<?php
include '../conn.php';

if (isset($_GET['student_id'])) {
    $studentId = $_GET['student_id'];

    $query = "SELECT s.stdname, f.name AS faculty_name, l.name AS level_name, s.current_cost, s.total_cost
              FROM student s
              JOIN faculty f ON s.faculty_id = f.id
              JOIN level l ON s.level_id = l.id
              WHERE s.id = $studentId";

    $result = mysqli_query($conn, $query);

    if ($row = mysqli_fetch_assoc($result)) {
        $stdname = $row['stdname'];
        $facultyName = $row['faculty_name'];
        $level_name = $row['level_name'];
        $currentCost = $row['current_cost'];
        $totalCost = $row['total_cost'];
     
    } else {
       
        exit();
    }
} else {
  
    exit();
}



?>

<!DOCTYPE html>
<html lang="ar" dir="rtl" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>نظام التأمين الصحي -  فتح تذكرة جديدة</title>
    <!-- Include Bootstrap styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/fontawesome.css">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/owl.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet"href="../css/swiper-bundle.min.css"/>
    <script src="../js/jquery.min.js"></script>
     <script src="../js/bootstrap.min.js"></script>
    <script src="../js/isotope.min.js"></script>
    <script src="../js/owl-carousel.js"></script>
     <script src="../js/counter.js"></script>
     <script src="../js/custom.js"></script>
     <script src="../js/jquery-ui.js"></script>
      <style>

<style>
        body {
            background-color: #f4f4f4;
            font-family: 'Cairo', sans-serif;
        }

        .custom-table {
            width: 100%;
            margin: auto;
            font-size: 18px;
            border: 2px solid #dddddd;
            border-radius: 10px;
            overflow: hidden;
        }

        .custom-table th,
        .custom-table td {
            border-bottom: 1px solid #dddddd;
            padding: 10px;
        }
    </style>

<script>
$(document).ready(function () {
    $('#categoryDropdown').change(function () {
        $('#category_id').val($(this).val());
    });

    $('#addTicketForm').submit(function (event) {
        event.preventDefault(); 

        var formData = new FormData(this);
        var ticketNotes = $('#ticket_notes').val(); 
        formData.append('ticket_notes', ticketNotes); 

        $.ajax({
            type: 'POST',
            url: '/Health/ticket/add_ticket.php',
            data: formData,
            processData: false,
            contentType: false,
            success: function (jsonResponse) {
    var response = JSON.parse(jsonResponse);
    if (response.success) {
        $("#successMessage").html(response.message).fadeIn();

        if (response.lastTicketId) {
            setTimeout(function () {
                window.open('generate_pdf.php?ticketId=' + response.lastTicketId, '_blank');
            }, 3000);
        }
    } else {
        $("#errorMessage").html(response.message).fadeIn();
    }
},
            error: function () {
                $("#errorMessage").html('حدث خطأ أثناء إرسال البيانات.').fadeIn().delay(3000).fadeOut();
            }
        });
    });
});

    
</script>

</head>
<body>
    <?php include('../header.php'); ?>
    <section style="margin: 10px 0;">
        <div class="container mt-5">
            <h2>فتح تذكرة جديدة</h2>
            <br/><br/>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped custom-table">
                        <tbody>
                            <tr>
                                <th scope="row">الإسم</th>
                                <td><?php echo $stdname; ?></td>
                                <th scope="row">الكلية</th>
                                <td><?php echo $facultyName; ?></td>
                            </tr>
                            <tr>
                            <th scope="row">المبلغ المتاح</th>
                                <td><?php echo $totalCost; ?></td>
                                <th scope="row">المبلغ المستخدم</th>
                                <td><?php echo $currentCost; ?></td>
                                
                            </tr>
                           
                        </tbody>
                    </table>
                    
                </div>
                
            </div>
            <br/>
            <div class="row">
            <div class="col-md-5 ">
            <label for="categoryDropdown" class="form-label">الجهة المحول إليها:</label>
                        <select class="form-select" id="categoryDropdown" name="categoryDropdown" value=""  required>
                        <option value="" selected disabled>اختر الجهة</option>
                            <?php
                                include '..\conn.php';

                                $categoryQuery = "SELECT id, name FROM category";
                                $categoryResult = mysqli_query($conn, $categoryQuery);

                                while ($categoryRow = mysqli_fetch_assoc($categoryResult)) {
                                    echo "<option value='{$categoryRow['id']}'>{$categoryRow['name']}</option>";
                                }

                                // Close database connection
                                mysqli_close($conn);
                            ?>
                        </select>
                        </div>  
                        <div class="col-md-5">
                        <label for="TicketNotes" class="form-label">ملحوظات</label>
                        <input type="text" class="form-control" name="ticket_notes" id="ticket_notes"  required>
                            </div><div class="col-md-5">
                        <form action="/Health/ticket/add_ticket.php" method="post" id="addTicketForm" >                            <input type="hidden" name="student_id" value="<?php echo $studentId; ?>">
                            <input type="hidden" name="category_id" id="category_id" value="">   <input type="hidden" name="ticket_notes" id="ticket_notes" value="">                    

                            <button type="submit" class="btn btn-primary submit-btn">حفظ التذكرة</button>

                            
                           
                           
</form>
                            </div>  </div>
        </div>
        
                       
       <br />

        <div id="successMessage" name="successMessage" class="alert alert-success" style="display:none;">
        </div>
        
        <div id="errorMessage" name="errorMessage" class="alert alert-danger" style="display:none;"></div>
    </section>   
   
   

  <?php include('../footer.php'); ?>