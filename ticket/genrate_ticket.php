<?php
include '../conn.php';

$ticketId = mysqli_real_escape_string($conn, $_GET['ticketId']);

$query = "SELECT t.id, t.closed, s.stdname AS student_name, c.name AS category_name, t.date, t.note
          FROM ticket t
          JOIN student s ON t.student_id = s.id
          JOIN category c ON t.category_id = c.id
          WHERE t.id = '$ticketId'";

$result = mysqli_query($conn, $query);

$ticket = mysqli_fetch_assoc($result);

mysqli_close($conn);

require_once('../tcpdf/tcpdf.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetTitle('التأمين الصحى لجامعة المنوفية الأهلية');

$lg = Array();
$lg['a_meta_charset'] = 'UTF-8';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';



$pdf->setLanguageArray($lg);
$pdf->SetFont('aealarabiya', '', 12);

$pdf->AddPage();

$pdf->Cell(0, 10, 'طباعة التذكرة', 0, 1, 'C');
$pdf->Ln(10);
$pdf->Cell(0, 10, 'رقم التذكرة: ' . $ticket['id'], 0, 1);
$pdf->Cell(0, 10, 'اسم الطالب: ' . $ticket['student_name'], 0, 1);
$pdf->Cell(0, 10, 'الجهة: ' . $ticket['category_name'], 0, 1);

$pdf->setRTL(true);
$pdf->SetFont('aealarabiya', '', 18);

$pdf->Output('ticket_details.pdf', 'I');
?>
