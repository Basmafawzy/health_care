<!DOCTYPE html>
<html lang="ar" dir="rtl" >

<?php
    require_once "../conn.php";
    if(isset($_POST["name"])){
        $name = $_POST['name'];
        $sql = "UPDATE category SET `name`= '$name' WHERE id= ".$_GET["id"];
        if (mysqli_query($conn, $sql)) {
            header("location: index.php");
        } else {
            echo "حدث خطأ.";
        }
    }
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>نظام التأمين الصحي - الجهات الصحية</title>
    <!-- Include Bootstrap styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/fontawesome.css">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/owl.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet"href="../css/swiper-bundle.min.css"/>
    <script src="../js/jquery.min.js"></script>
     <script src="../js/bootstrap.min.js"></script>
    <script src="../js/isotope.min.js"></script>
    <script src="../js/owl-carousel.js"></script>
     <script src="../js/counter.js"></script>
     <script src="../js/custom.js"></script>
     <style>

body {
    background-color: #f4f4f4;
    font-family: 'Cairo', sans-serif;

}



</style>
</head>
<body>
    <?php include('../header.php'); ?>
    <section>
        <h1 style="text-align: center;margin: 50px 0;">تحديث الجهات الصحية</h1>
        <div class="container">
            <?php 
                require_once "../conn.php";
                $sql_query = "SELECT * FROM category WHERE id = ".$_GET["id"];
                if ($result = $conn ->query($sql_query)) {
                    while ($row = $result -> fetch_assoc()) { 
                        $Id = $row['Id'];
                        $Name = $row['Name'];
                      
            ?>
                            <form action="updatecategory.php?id=<?php echo $Id; ?>" method="post">
                                <div class="row">
                                        <div class="form-group col-lg-10 ">
                                            <label for="">الإسم</label>
                                            <input type="text" name="name" id="name" class="form-control" value="<?php echo $Name ?>" required>
                                        </div>
                                        
                                        <div class="form-group col-lg-2" style="display: grid;align-items:  flex-end;">
                                            <input type="submit" name="submit" id="submit" class="btn btn-primary" value="تحديث">
                                        </div>
                                </div>
                            </form>
            <?php 
                    }
                }
            ?>
        </div>
    </section>

    <?php include('../footer.php'); ?>