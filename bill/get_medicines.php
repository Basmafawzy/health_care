<?php
include '../conn.php';

$medicineQuery = "SELECT id, name FROM medicine";
$medicineResult = mysqli_query($conn, $medicineQuery);


$options = '';
while ($medicine = mysqli_fetch_assoc($medicineResult)) {
    $options .= '<option value="' . $medicine['id'] . '">' . $medicine['name'] . '</option>';
}

echo $options;
?>
