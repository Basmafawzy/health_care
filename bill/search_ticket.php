<?php
include '../conn.php';

$searchTerm = mysqli_real_escape_string($conn, $_GET['searchTerm']);

$query = "SELECT t.id, t.closed, s.stdname AS student_name, c.name AS category_name, t.date, t.note
          FROM ticket t
          JOIN student s ON t.student_id = s.id
          JOIN category c ON t.category_id = c.id
          WHERE t.closed=0 AND (t.id LIKE '%$searchTerm%' OR s.stdname LIKE '%$searchTerm%' OR c.name LIKE '%$searchTerm%')";

$result = mysqli_query($conn, $query);

$html = '';

while ($row = mysqli_fetch_assoc($result)) {
    $html .= "<tr>";
    $html .= "<td>{$row['id']}</td>";
    $html .= "<td>{$row['student_name']}</td>";
    $html .= "<td>{$row['category_name']}</td>";
  
    $html .= "<td>" . date('d-m-Y', strtotime($row['date'])) . "</td>";
    $html .= "<td><a href='add_invoice.php?ticket_id={$row['id']}' class='btn btn-primary'>إضافة فاتورة</a></td>";


    $html .= "</tr>";
}

echo $html;

mysqli_close($conn);
?>
