<?php
include '../conn.php';
include('../header.php');

$MedicinesQuery = "SELECT *  FROM `medicine`";
$MedicineResult = mysqli_query($conn, $MedicinesQuery);
$servicesQuery = "SELECT id, name FROM service";
$servicesResult = mysqli_query($conn, $servicesQuery);

$ticketId = isset($_GET['ticket_id']) ? intval($_GET['ticket_id']) : 0;
$ticketQuery = "SELECT t.id, s.stdname AS student_name, s.total_cost, s.current_cost, t.date AS ticket_date
                FROM ticket t
                JOIN student s ON t.student_id = s.id
                WHERE t.id = $ticketId";
$ticketResult = mysqli_query($conn, $ticketQuery);

if (!$ticketResult) {
    die('Error in SQL query: ' . mysqli_error($conn));
}

if ($ticketData = mysqli_fetch_assoc($ticketResult)) {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $ticket_id = $ticketData['id'];

        mysqli_autocommit($conn, false);

        $totalInvoiceValue = array_sum($_POST['value']);
        $updateResult = false;

        if ($totalInvoiceValue <= $ticketData['total_cost']) {
            for ($i = 0; $i < count($_POST['service_id']); $i++) {
                $invoice_number = mysqli_real_escape_string($conn, $_POST['invoice_number'][$i]);
                $service_id = $_POST['service_id'][$i];
                $value = $_POST['value'][$i];
                $note=  mysqli_real_escape_string($conn, $_POST['note'][$i]);
               
               $medicines = isset($_POST['medicine_id']) ? implode(' - ', $_POST['medicine_id']) : '';
                 $date = 'NOW()';

                $insertQuery = $conn->prepare("INSERT INTO invoice (invoice_number, service_id, invoice_date, value, ticket_id, medicine_list,note) 
                VALUES (?, ?, $date, ?, ?, ?,?)");

                $insertQuery->bind_param("ssdiss", $invoice_number, $service_id, $value, $ticket_id, $medicines,$note);
                $insertResult = $insertQuery->execute();

                if ($insertResult) {
                    $newTotalCost = $ticketData['total_cost'] - $totalInvoiceValue;
                    $newCurrentCost = $ticketData['current_cost'] + $totalInvoiceValue;

                    $updateStudentQuery = $conn->prepare("UPDATE student s
                                       JOIN ticket t ON s.id = t.student_id
                                      SET s.total_cost = ?, s.current_cost = ?
                                      WHERE t.id = ?");
                    $updateStudentQuery->bind_param("ddi", $newTotalCost, $newCurrentCost, $ticketData['id']);
                    $updateResult = $updateStudentQuery->execute();
                    $updatedTicketQuery = "SELECT s.total_cost, s.current_cost
                    FROM ticket t
                    JOIN student s ON t.student_id = s.id
                    WHERE t.id = $ticket_id";
                    $updatedTicketResult = mysqli_query($conn, $updatedTicketQuery);

                     if ($updatedTicketResult) {
                     $updatedTicketData = mysqli_fetch_assoc($updatedTicketResult);

                
                   mysqli_free_result($updatedTicketResult);
                       }

                $message = '';
                    if ($updateResult) {
                        $message .= '<div class="success-message text-center" >تم تحديث الرصيد الخاص بالطالب' ;
                        $message .= '&nbsp;&nbsp;<form action="close_ticket.php" method="post" class="d-inline-block">';
                        $message .= '<input type="hidden" name="ticket_id" value="' . $ticketData['id'] . '">';
                        $message .= '<button type="submit" class="btn btn-danger ">إغلاق التذكرة</button>';
                        $message .= '</form></div>';                          
             
                } else {
                    $message .= '<div class="error-message text-center">حدث مشكلة عند تحديث حساب الطالب ' . $updateStudentQuery->error . '</div>';
                    }

                    $updateStudentQuery->close();
                } else {
                    $message = '';

                    $message .= '<div class="error-message text-center">حدث مشكلة في إضافة الفاتورة: ' . $insertQuery->error . '</div>';
                }

                $insertQuery->close();
            }

            if (!mysqli_commit($conn)) {
                $message = '';
                $message .= '<div class="error-message text-center">حدث خطأ.</div>';
                mysqli_rollback($conn); 
            }

            mysqli_autocommit($conn, true);
        } else {
            $updatedTicketQuery = "SELECT s.total_cost, s.current_cost
            FROM ticket t
            JOIN student s ON t.student_id = s.id
            WHERE t.id = $ticket_id";
            $updatedTicketResult = mysqli_query($conn, $updatedTicketQuery);

             if ($updatedTicketResult) {
             $updatedTicketData = mysqli_fetch_assoc($updatedTicketResult);

        
           mysqli_free_result($updatedTicketResult);
               }
            $message = '';
            $message .= '<div class="error-message text-center ">لا يوجد رصيد كافي للطالب لإضافة الفاتورة</div>';
        }
    }
} else {
    die("لا يوجد رصيد كافي لإضافة فاتورة.");
}

mysqli_close($conn);
?>



<!DOCTYPE html>
<html lang="ar" dir="rtl" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>نظام التأمين الصحي -  إضافة فاتورة</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/fontawesome.css">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/owl.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet"href="../css/swiper-bundle.min.css"/>

    <script src="../css/jquery.min.js"></script>
    <link href="../css/select2.min.css" rel="stylesheet" />
    <script src="../css/select2.min.js"></script>

    </head>
   
      <script src="../js/bootstrap.min.js"></script>

    <script src="../js/isotope.min.js"></script>
    <script src="../js/owl-carousel.js"></script>
     <script src="../js/counter.js"></script>
     <script src="../js/custom.js"></script>
 

      <style>

body {
    background-color: #f4f4f4;
    font-family: 'Cairo', sans-serif;

}

#file {
        border: 1px solid #ced4da; 
        border-radius: 0.25rem; 
        padding: 0.375rem 0.75rem; 
    }
    #autocomplete-container {
        width: 100%;
        background-color: #fff;
        max-height: 200px;
        overflow-y: auto;
        z-index: 1000;
    }

    .ui-autocomplete-category {
        font-weight: bold;
        padding: 8px;
        background-color: #f5f5f5;
    }

    .ui-menu-item {
        padding: 8px;
        cursor: pointer;
    }

    .ui-menu-item:hover {
        background-color: #f0f0f0;
    }
    .label-container {
    border: 1px solid #3498db; 
    padding: 10px;
    margin-bottom: 10px;
    background-color: #ecf0f1; 
}

.label-value {
    padding: 8px;
    background-color: #ffffff; 
    border: 1px solid #95a5a6; 
    color: #2c3e50; 
}
.messages-container {
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
    padding: 10px;
    background-color: #f8d7da; 
    color: #721c24; 
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 18px;
}

.success-message {
    background-color: #d4edda; 
    color: #155724; 
}

.error-message,
.success-message {
    border: 1px solid #c3e6cb; 
    border-radius: 5px; 
    padding: 15px; 
    margin: 10px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); 
    font-size: 18px;
}
.custom-table {
            width: 100%;
            margin: auto;
            font-size: 18px;
            border: 2px solid #dddddd;
            border-radius: 10px;
            overflow: hidden;
        }

        .custom-table th,
        .custom-table td {
            border-bottom: 1px solid #dddddd;
            padding: 10px;
        }
</style>
<script>


    function addService() {
        var newService = $("#serviceTemplate").clone();
        var serviceId = Date.now(); // Unique identifier for the service

        newService.attr("id", "service_" + serviceId);

        newService.find("select, input").val('');

        var deleteButton = $('<button type="button" class="btn btn-danger" onclick="deleteService(' + serviceId + ')">-</button>');
        deleteButton.hide(); // Hide delete button initially
        newService.append(deleteButton);

        newService.css("display", "block");
        $("#servicesContainer").append(newService);

        // Show delete button after the first add
        if ($("#servicesContainer").children().length > 1) {
            deleteButton.show();
        }
    }

    function deleteService(serviceId) {
        $("#service_" + serviceId).remove();
    }

    $(document).ready(function () {
        $('.js-example-basic-multiple').select2();
        addService();
       
    });

    // Medicne Part 
$(document).ready(function() {
    $('select[name="service_id[]"]').change(function() {
        if ($(this).val() == '1') {
            $('#medicineList').show();
           
        } else {
            $('#medicineList').hide();
        }
    });
});



</script>



<body>

<?php ?>

<div class="container mt-5">
 <div class="row">
    <div class="col-md-12">
        <table class="table table-striped custom-table">
            <tbody>
                <tr>
                    <th scope="row">رقم التذكرة</th>
                    <td><?php echo $ticketData['id']; ?></td>
                    <th scope="row">اسم الطالب</th>
                    <td><?php echo $ticketData['student_name']; ?></td>
                </tr>
                <tr>
                    <th scope="row">الرصيد المتاح</th>
                    <td>
                        <?php echo $_SERVER['REQUEST_METHOD'] == 'POST' ? $updatedTicketData['total_cost'] : $ticketData['total_cost']; ?>
                    </td>
                    <th scope="row">الرصيد المستهلك</th>
                    <td>
                        <?php echo $_SERVER['REQUEST_METHOD'] == 'POST' ? $updatedTicketData['current_cost'] : $ticketData['current_cost']; ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<hr>
    <h2>إضافة فاتورة</h2>
    <br />
   


  
    <form method="POST" action=""  id="invoiceForm">
        <input type="hidden" name="ticket_id" value="<?php echo $ticketData['id']; ?>">

        <div id="servicesContainer" class="row">
        </div>

        <button type="button" class="btn btn-success" onclick="addService()">+</button>
        <button type="submit" class="btn btn-primary">إصدار فاتورة</button>

    </form>

    <div id="serviceTemplate" style="display:none;">
    <div class="row">
    <div class="col-md-2">
        <div class="mb-2">
            <label for="invoice_number" class="form-label">رقم الفاتورة</label>
            <input type="text" class="form-control" name="invoice_number[]" placeholder="رقم الفاتورة" >
        </div>
    </div>
    <div class="col-md-2">
      
            <label for="service_id" class="form-label">نوع الصنف</label>
            <select class="form-select" name="service_id[]" required>
                <?php
                mysqli_data_seek($servicesResult, 0); 
                while ($service = mysqli_fetch_assoc($servicesResult)) {
                    echo "<option value='{$service['id']}'>{$service['name']}</option>";
                }
                ?>
            </select>
        </div>
        <div class="col-md-7">
    <div id="medicineList" name="medicineList" style="display: none;">
    <label for="medicine" class="form-label">اسم الدواء</label><br/>
    <select class="form-control select2" id="medicine_id" name="medicine_id[]" multiple="multiple" style="width: 350px;">
        <?php                                  
    while ($row = $MedicineResult->fetch_assoc()) {
        echo "<option>".$row["name"]."</option>";
    }
    ?>
</select>

    </div>
</div>


        <div  class="mb-3 col-md-2">
            <label for="value" class="form-label">القيمة</label>
            <input type="text" class="form-control" name="value[]" placeholder="القيمة" required>
        </div>  <div  class="mb-4 col-md-5">
            <label for="value" class="form-label">ملحوظات</label>
            <input type="text" class="form-control" name="note[]" placeholder="ملحوظات" required>
        </div>
   
</div>

    </div><?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    echo $message;
}
?>
    </div>
    
    <script type="text/javascript">
   // In your Javascript (external .js resource or <script> tag)
   $(document).ready(function() {
      $('#medicine_id').select2();
      
   });
</script>

<input type="hidden" id="medicineList" name="medicineList" value="">
</body>
</html>

<?php include('../footer.php'); ?>


