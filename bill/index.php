<?php
include '../conn.php';
include('../header.php');
$recordsPerPage = isset($_GET['perPage']) ? $_GET['perPage'] : 10;

$currentPage = isset($_GET['page']) ? $_GET['page'] : 1;

$offset = ($currentPage - 1) * $recordsPerPage;

$searchTerm = '';

if (isset($_GET['searchInput'])) {
    $searchTerm = mysqli_real_escape_string($conn, $_GET['searchInput']);
}

$query = "SELECT t.id, t.closed, s.stdname AS student_name, c.name AS category_name, t.date, t.note
          FROM ticket t
          JOIN student s ON t.student_id = s.id
          JOIN category c ON t.category_id = c.id
          WHERE t.closed=0 ORDER BY t.id DESC" ;

if (!empty($searchTerm)) {
    $query .= " WHERE t.id LIKE '%$searchTerm%' OR s.stdname LIKE '%$searchTerm%' OR c.name LIKE '%$searchTerm%'";
}

$query .= " LIMIT $offset, $recordsPerPage";

$result = mysqli_query($conn, $query);

if (!$result) {
    die('Error in SQL query: ' . mysqli_error($conn));
}

?>

<!DOCTYPE html>
<html lang="ar" dir="rtl" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>نظام التأمين الصحي -  إصدار فاتورة</title>
    <!-- Include Bootstrap styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/fontawesome.css">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/owl.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet"href="../css/swiper-bundle.min.css"/>
    <script src="../js/jquery.min.js"></script>
     <script src="../js/bootstrap.min.js"></script>
    <script src="../js/isotope.min.js"></script>
    <script src="../js/owl-carousel.js"></script>
     <script src="../js/counter.js"></script>
     <script src="../js/custom.js"></script>
     <script src="../js/jquery-ui.js"></script>

      <style>

body {
    background-color: #f4f4f4;
    font-family: 'Cairo', sans-serif;

}

#file {
        border: 1px solid #ced4da;
        border-radius: 0.25rem; 
        padding: 0.375rem 0.75rem; 
    }
    #autocomplete-container {
        width: 100%;
        background-color: #fff;
        max-height: 200px;
        overflow-y: auto;
        z-index: 1000;
    }

    .ui-autocomplete-category {
        font-weight: bold;
        padding: 8px;
        background-color: #f5f5f5;
    }

    .ui-menu-item {
        padding: 8px;
        cursor: pointer;
    }

    .ui-menu-item:hover {
        background-color: #f0f0f0;
    }
</style>

<script>
 $(document).ready(function () {
    $("#searchButton").on("click", function () {
        var searchTerm = $("#searchInput").val();

        $.ajax({
            url: "search_ticket.php",
            type: "GET",
            data: { searchTerm: searchTerm },
            success: function (data) {
                $("#ticketTableBody").html(data);
            },
            error: function () {
                alert("حدث مشكلة عند البحث.");
            }
        });
    });

    $("#perPage").on("change", function () {
        this.form.submit();
    });
});


</script>

<script>
     

   

$(document).ready(function () {
    $("#searchButton").on("click", function () {
        var searchTerm = $("#searchInput").val();

        $.ajax({
            url: "search_ticket.php", 
            type: "GET",
            data: { searchTerm: searchTerm },
            success: function (data) {
                $("#ticketTableBody").html(data);
            },
            error: function () {
                alert("حدث مشكلة عند البحث.");
            }
        });
    });
});
</script>
</head>
<body>
    <?php  ?>
 

            <section style="margin: 10px 0;">
        <div class="container mt-5">
            <h2>إصدار فاتورة</h2>
            <br />

            <div class="mb-3">
                <label for="searchInput" class="form-label">بحث</label>
                <input type="text" class="form-control" name="searchInput" id="searchInput" placeholder="البحث">
                <div id="autocomplete-container"></div>
            </div>
            <div class="mb-3">
                <button type="button" class="btn btn-primary" id="searchButton" name="searchButton">ابحث</button>
            </div>

            <form method="GET" action="">
                <label for="perPage">عدد السجلات في الصفحة:</label>
                <select name="perPage" id="perPage" onchange="this.form.submit()">
                    <option value="10" <?php echo ($recordsPerPage == 10) ? 'selected' : ''; ?>>10</option>
                    <option value="20" <?php echo ($recordsPerPage == 20) ? 'selected' : ''; ?>>20</option>
                </select>
            </form>

            <table class="table table-striped dataTable" >
                <thead>
                    <tr>
                        <th class="sortable">رقم التذكرة</th>
                        <th class="sortable">اسم الطالب</th>
                        <th class="sortable">الجهة</th>
                        <th class="sortable">تاريخ الإنشاء</th>
                        <th class="sortable">العملية</th>
                       
                    </tr>
                </thead>
                <tbody id="ticketTableBody" name="ticketTableBody">
                    <?php
                    while ($row = mysqli_fetch_assoc($result)) {
                        echo "<tr>";
                        echo "<td>{$row['id']}</td>";
                        echo "<td>{$row['student_name']}</td>"; 
                        echo "<td>{$row['category_name']}</td>"; 
                        echo "<td>" . date('d-m-Y', strtotime($row['date'])) . "</td>";
                        echo "<td><a href='add_invoice.php?ticket_id={$row['id']}' class='btn btn-primary'>إضافة فاتورة</a></td>";
    
                echo "</tr>";
           
                        echo "</tr>";
                    }
                    $query = "SELECT COUNT(*) AS total FROM ticket t  WHERE t.closed=0";
                    $result = mysqli_query($conn, $query);
                    $row = mysqli_fetch_assoc($result);
                    $totalPages = ceil($row['total'] / $recordsPerPage);
                    mysqli_close($conn);
                    ?>
                </tbody>
            </table>

            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <?php
                    for ($i = 1; $i <= $totalPages; $i++) {
                        echo "<li class='page-item " . (($currentPage == $i) ? 'active' : '') . "'>";
                        echo "<a class='page-link' href='?page=$i&perPage=$recordsPerPage'>$i</a>";
                        echo "</li>";
                    }
                    ?>
                </ul>
            </nav>
        </div>
    </section>

<?php include('../footer.php'); ?>