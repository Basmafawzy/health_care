<?php 
// Load the database configuration file 
include_once '../conn.php'; 

// Fetch records from database 
$query = "SELECT i.id, i.invoice_number, s.name AS service_name, i.invoice_date, i.value,i.ticket_id , c.name as ticket_category, t.id AS ticket_id, st.stdname AS student_name ,f.name As faculty_name, st.current_cost As student_balance , st.total_cost as student_remaing , medicine_list,i.note
FROM invoice i
JOIN service s ON i.service_id = s.id
LEFT JOIN ticket t ON i.ticket_id = t.id
LEFT JOIN category c on c.Id=t.category_id
LEFT JOIN student st ON t.student_id = st.id 
LEFT JOIN faculty f ON  st.faculty_id = f.id ORDER BY i.invoice_date DESC";

$result = mysqli_query($conn, $query);

$queryTotalBalance = "SELECT SUM(value) as TotalInvoices from invoice";
$TotalBalance = mysqli_query($conn, $queryTotalBalance);
if ($TotalBalance->num_rows > 0) {
    while($row = $TotalBalance->fetch_assoc()) {
        $total = $row["TotalInvoices"];
    }}



// Excel file name for download 
$fileName = "فواتير _" . date('Y-m-d') . ".xls"; 
 


$table = '<table>

<tr></tr>
    <tr>
    <td></td>
    

    <td style="font-weight:bold;">تصميم وحدة الخدمات الإلكترونية جامعة المنوفية الأهلية </td>
    <td>'.date("l jS \of F Y h:i:s A").'</td>
    <td style="font-weight:bold;font-size:25px;background-color:rgb(0, 204, 0);">'.$total.'</td>

    <td style="font-weight:bold;font-size:25px;background-color:rgb(0, 204, 0);">إجمالى  مبلغ الفواتير  </td>
    </tr>

<tr style="font-weight:bold;font-size:25px;background-color:gold;">
<th>رقم التذكرة</th>
<th>اسم الطالب</th>
<th>الرصيد المستهلك</th>
<th>الرصيد المتبقي</th>  
<th>الجهة</th>
<th>التاريخ</th>
<th>الصنف</th>
<th>معلومات</th>
<th>قيمة</th>
<th>ملحوظات</th>
<th>الكلية</th>
</tr>';
while($row = mysqli_fetch_array($result)){
    $table.='<tr>
    <td>'.$row['ticket_id'].'</td>
    <td>'.$row['student_name'].'</td>
    <td>'.$row['student_balance'].'</td>
    <td>'.$row['student_remaing'].'</td>
    <td>'.$row['ticket_category'].'</td>
    <td>'.$row['invoice_date'].'</td>
    <td>'.$row['service_name'].'</td>
    <td>'.$row['medicine_list'].'</td>
    <td>'.$row['value'].'</td>
    <td>'.$row['note'].'</td>
    <td>'.$row['faculty_name'].'</td>
    </tr>';
}
    // $table.='<tr></tr>
    // <tr>
    // <td></td>
    // <td></td>

    // <td></td>

    // <td style="font-weight:bold;font-size:25px;background-color:gold;">'.$total.'</td>

    // <td style="font-weight:bold;font-size:25px;background-color:gold;">إجمالى  مبلغ الفواتير  </td>
    // </tr>';


    $table.='</table>';


    header("Content-Encoding: utf-8");
    header("Content-type:application/vnd.ms-excel; charset=utf-8");
 
    header("Content-Disposition: attachment; filename=\"$fileName\""); 

    echo $table;
    ?>

