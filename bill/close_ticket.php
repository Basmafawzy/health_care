<?php
include '../conn.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $ticket_id = intval($_POST['ticket_id']);

    $closeTicketQuery = $conn->prepare("UPDATE ticket SET closed = 1 WHERE id = ?");
    $closeTicketQuery->bind_param("i", $ticket_id);
    $closeTicketResult = $closeTicketQuery->execute();

    if ($closeTicketResult) {
        header("Location: index.php"); 
        exit();
    } else {
        echo "Error closing the ticket: " . $closeTicketQuery->error;
    }

    $closeTicketQuery->close();
}

mysqli_close($conn);
?>
