<?php
include '../conn.php';
include('../header.php');

$recordsPerPage = isset($_GET['perPage']) ? $_GET['perPage'] : 10;
$currentPage = isset($_GET['page']) ? $_GET['page'] : 1;
$offset = ($currentPage - 1) * $recordsPerPage;
$searchTerm = '';

if (isset($_GET['searchInput'])) {
    $searchTerm = mysqli_real_escape_string($conn, $_GET['searchInput']);
}

$query = "SELECT i.id, i.invoice_number, s.name AS service_name, i.invoice_date, i.value,i.ticket_id , c.name as ticket_category, t.id AS ticket_id, st.stdname AS student_name , st.current_cost As student_balance , st.total_cost as student_remaing , medicine_list,i.note
FROM invoice i
JOIN service s ON i.service_id = s.id
LEFT JOIN ticket t ON i.ticket_id = t.id
LEFT JOIN category c on c.Id=t.category_id
LEFT JOIN student st ON t.student_id = st.id ORDER BY i.invoice_date DESC";

if (!empty($searchTerm)) {
    $query .= " WHERE st.stdname LIKE '%$searchTerm%' OR i.invoice_number LIKE '%$searchTerm%' OR s.name LIKE '%$searchTerm%'";
}

$query .= " LIMIT $offset, $recordsPerPage";

$result = mysqli_query($conn, $query);

if (!$result) {
    die('Error in SQL query: ' . mysqli_error($conn));
}

$queryTotal = "SELECT COUNT(*) AS total FROM invoice i LEFT JOIN ticket t ON i.ticket_id = t.id LEFT JOIN student st ON t.student_id = st.id";

$totalResult = mysqli_query($conn, $queryTotal);
$row = mysqli_fetch_assoc($totalResult);
$totalPages = ceil($row['total'] / $recordsPerPage);

mysqli_free_result($totalResult);


?>

<!DOCTYPE html>
<html lang="ar" dir="rtl" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>نظام التأمين الصحي -  قائمة الفواتير</title>
    <!-- Include Bootstrap styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/fontawesome.css">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/owl.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet"href="../css/swiper-bundle.min.css"/>
    <link rel="stylesheet"href="../css/jquery.dataTables.css"/>
    <script src="../js/jquery.min.js"></script>
     <script src="../js/bootstrap.min.js"></script>
    <script src="../js/isotope.min.js"></script>
    <script src="../js/owl-carousel.js"></script>
     <script src="../js/counter.js"></script>
     <script src="../js/custom.js"></script>
     <script src="../js/jquery-ui.js"></script>
     <script src="../js/jquery.dataTables.js"></script>
      <style>

<style>
        body {
            background-color: #f4f4f4;
            font-family: 'Cairo', sans-serif;
        }

        .custom-table {
            width: 100%;
            margin: auto;
            font-size: 18px;
            border: 2px solid #dddddd;
            border-radius: 10px;
            overflow: hidden;
        }

        .custom-table th,
        .custom-table td {
            border-bottom: 1px solid #dddddd;
            padding: 10px;
        }
    </style>
<script>
   $(document).ready(function() {
        $('#yourTableId').DataTable({
        });
    });
    
$(document).ready(function () {
    $("#searchButton").on("click", function () {
        var searchTerm = $("#searchInput").val();

        $.ajax({
            url: "search_invoice _student.php", 
            type: "GET",
            data: { searchTerm: searchTerm },
            success: function (data) {
                $("#ticketTableBody").html(data);
            },
            error: function () {
                alert("حدث مشكلة عند البحث.");
            }
        });
    });
});
</script>
</head>
<body>
    <?php  ?>
 

            <section style="margin: 10px 0;">
            <div class="container mt-5">

            <div class="mb-3">
                <label for="searchInput" class="form-label">بحث</label>
                <input type="text" class="form-control" name="searchInput" id="searchInput" placeholder="البحث">
                <div id="autocomplete-container"></div>
            </div> 
            <div class="mb-3">
                <button type="button" class="btn btn-primary" id="searchButton" name="searchButton">ابحث</button>
 
                </div>

                

            <form method="GET" action="">
                <label for="perPage">عدد السجلات في الصفحة:</label>
                <select name="perPage" id="perPage" onchange="this.form.submit()">
                    <option value="10" <?php echo ($recordsPerPage == 10) ? 'selected' : ''; ?>>10</option>
                    <option value="20" <?php echo ($recordsPerPage == 20) ? 'selected' : ''; ?>>20</option>
                </select>
            </form>

            <table class="table table-striped dataTable" >
                <thead>
                    <tr>
                        
                        <th class="sortable">رقم التذكرة</th>
                        <th class="sortable">اسم الطالب</th>
                        <!-- <th class="sortable">الرصيد المستهلك</th>
                         <th class="sortable">الرصيد المتبقي</th>  
                        <th class="sortable">الجهة</th>
                        <th class="sortable">التاريخ</th>
                        <th class="sortable">الصنف</th>
                        <th class="sortable">معلومات</th>
                        <th class="sortable">قيمة</th> -->
                        <th class="sortable">تقرير فواتير </th> 
                    </tr>
                </thead>
                <tbody id="ticketTableBody" name="ticketTableBody">
                    <?php
                   
while ($row = mysqli_fetch_assoc($result)) {
    echo "<tr>";
   echo "<td>{$row['ticket_id']}</td>";
    echo "<td>{$row['student_name']}</td>";
    echo "<td>

             <form method='GET' action='/Health/ticket/generate_pdf.php' target='_blank'>
             <input type='hidden' name='ticketId' value='{$row['id']}'>
            <button type='submit' class='btn btn-sm btn-primary'>طباعة </button>
            </form>
                        </td>";
    // echo "<td>{$row['student_balance']}</td>";
    // // echo "<td>{$row['student_remaing']}</td>";
    // echo "<td>{$row['ticket_category']}</td>";
    // echo "<td>" . date('d-m-Y', strtotime($row['invoice_date'])) . "</td>";
    // echo "<td>{$row['service_name']}</td>";
    // echo "<td>{$row['medicine_list']}</td>";
    // echo "<td>{$row['value']}</td>";
    // echo "<td>{$row['note']}</td>";
    echo "</tr>";
}
                  
                   
                    ?>
                </tbody>
            </table>

            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <?php
                    // Display page numbers
                    for ($i = 1; $i <= $totalPages; $i++) {
                        echo "<li class='page-item " . (($currentPage == $i) ? 'active' : '') . "'>";
                        echo "<a class='page-link' href='?page=$i&perPage=$recordsPerPage'>$i</a>";
                        echo "</li>";
                    }
                    ?>
                </ul>
            </nav>
            </div>
    </section>

<?php include('../footer.php'); ?>