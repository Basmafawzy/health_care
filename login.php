
<?php
   include('conn.php');
   session_start();
   
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      
      $myusername = mysqli_real_escape_string($conn,$_POST['username']);
      $mypassword = mysqli_real_escape_string($conn,$_POST['password']); 
      
      $sql = "SELECT id FROM admin WHERE username = '$myusername' and passcode = '$mypassword'";
      $result = mysqli_query($conn,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      $count = mysqli_num_rows($result);
      
     



		
      if($count == 1) {
           $login_session = $row['username'];
        
          $_SESSION['login_user'] = $myusername;
          
         
          header("location:index.php");
         }
        

      else {
        
        $error = "خطأ فى بيانات تسجيل  الدخول ";
    }
  }
  

  
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/fontawesome.css">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/owl.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet"href="../css/swiper-bundle.min.css"/>
    <script src="../js/jquery.min.js"></script>
     <script src="../js/bootstrap.min.js"></script>
    <script src="../js/isotope.min.js"></script>
    <script src="../js/owl-carousel.js"></script>
     <script src="../js/counter.js"></script>
     <script src="../js/custom.js"></script>
     <script src="../js/jquery-ui.js"></script>
    </head>

    <body>
	<section class="login">
		<div class="login_box">
			<div class="left">
				<div class="top_link"><a href="index.php"><img src="images/logo.png" alt="">الصفحة الرئيسية</a></div>
				<div class="contact">
					<form action="" method = "post">
						<h3>تسجيل الدخول </h3>
						<input type="text" placeholder="اسم المستخدم" name="username"/>
						<input type="password" placeholder="الرقم السري " name="password"/>
            <div >

            <?php if (!empty($error)) : ?>
            <div class="alert alert-danger mt-3"><?php echo $error; ?></div>

        <?php endif; ?>

            </div>
						<button class="submit">دخول</button>
					</form>
          
				</div>
			</div>
			<div class="right">
				<div class="right-text">
					<h2>جامعة المنوفية الأهلية </h2>
					<h3>نظام التأمين الصحي </h3>
				</div>
				
			</div>
		</div>
	</section>
</body>
</html>
<style>
    img{
	width: 100%;
}
.login {
    height: 1000px;
    width: 100%;
    background: radial-gradient(#076431, #332042);
    position: relative;
}
.login_box {
    width: 1050px;
    height: 600px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
    background: #fff;
    border-radius: 10px;
    box-shadow: 1px 4px 22px -8px #0004;
    display: flex;
    overflow: hidden;
}
.login_box .left{
  width: 41%;
  height: 100%;
  padding: 25px 25px;
  
}
.login_box .right{
  width: 59%;
  height: 100%  
}
.left .top_link a {
    color: #0e6b2a;
    font-weight: 400;
}
.left .top_link{
  height: 20px
}
.left .contact{
	display: flex;
    align-items: center;
    justify-content: center;
    align-self: center;
    height: 100%;
    width: 73%;
    margin: auto;
}
.left h3{
  text-align: center;
  margin-bottom: 40px;
}
.left input {
    border: none;
    width: 80%;
    margin: 15px 0px;
    border-bottom: 1px solid #4f30677d;
    padding: 7px 9px;
    width: 100%;
    overflow: hidden;
    background: transparent;
    font-weight: 600;
    font-size: 14px;
}

.left{
    background: (-45deg, #dcd7e0, #fff);
   
}
.submit {
    border: none;
    padding: 15px 70px;
    border-radius: 8px;
    display: block;
    margin: auto;
    margin-top: 120px;
    background: #1e814a;
    color: #fff;
    font-weight: bold;
    -webkit-box-shadow: 0px 9px 15px -11px rgb(18, 119, 57);
    -moz-box-shadow: 0px 9px 15px -11px rgb(13, 116, 68);
    box-shadow: 0px 9px 15px -11px rgb(13, 82, 30);
}



.right {
	background: linear-gradient(212.38deg, rgba(57, 242, 187, 0.7) 0%, rgba(70, 189, 149, 0.71) 100%),url(images/background.webp);
	color: #fff;
	position: relative;
}

.right .right-text{
  height: 100%;
  position: relative;
  transform: translate(0%, 45%);
}
.right-text h2{
  display: block;
  width: 100%;
  text-align: center;
  font-size: 50px;
  font-weight: 500;
}
.right-text h3{
  display: block;
  width: 100%;
  text-align: center;
  font-size: 19px;
  font-weight: 400;
}

.right .right-inductor{
  position: absolute;
  width: 70px;
  height: 7px;
  background: #fff0;
  left: 50%;
  bottom: 70px;
  transform: translate(-50%, 0%);
}
.top_link img {
    width: 28px;
    padding-right: 7px;
    margin-top: -3px;
}




</style>