<!DOCTYPE html>
<html lang="ar" dir="rtl" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>نظام التأمين الصحي - الجهات الصحية</title>
    <!-- Include Bootstrap styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/fontawesome.css">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/owl.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet"href="../css/swiper-bundle.min.css"/>
    <script src="../js/jquery.min.js"></script>
     <script src="../js/bootstrap.min.js"></script>
    <script src="../js/isotope.min.js"></script>
    <script src="../js/owl-carousel.js"></script>
     <script src="../js/counter.js"></script>
     <script src="../js/custom.js"></script>
     <style>

body {
    background-color: #f4f4f4;
    font-family: 'Cairo', sans-serif;

}



</style>
</head>
<body>
    <?php include('../header.php'); ?>
<section>
        <h1 style="text-align: center;margin: 50px 0;">الادوية</h1>
        <div class="container">
            <form action="addmedicine.php" method="post">
               <div class="row">
                    <div class="form-group col-lg-10">
                        <label for="">الإسم</label>
                        <input type="text" name="name" id="name" class="form-control" required>
                    </div>
                    
                    <div class="form-group col-lg-2" style="display: grid;align-items:  flex-end;">
                        <input type="submit" name="submit" id="submit" value="إضافة"  class="btn btn-primary" > 
                    </div>
               </div>
            </form>
        </div>
    </section>

    <section style="margin: 50px 0;">
        <div class="container">
            <table class="table table-striped">
                <thead >
                  <tr>
                    <th scope="col">مسلسل</th>
                    <th scope="col">الإسم</th>
                 
                  </tr>
                </thead>
                <tbody>
                    <?php 
                        require_once "../conn.php";
                        $sql_query = "SELECT * FROM medicine";
                        if ($result = $conn ->query($sql_query)) {
                            while ($row = $result -> fetch_assoc()) { 
                                $Id = $row['id'];
                                $Name = $row['name'];
                    ?>
                    
                    <tr class="trow">
                        <td><?php echo $Id; ?></td>
                        <td><?php echo $Name; ?></td>
        
                    </tr>

                    <?php
                            } 
                        } 
                    ?>
                </tbody>
              </table>
        </div>
    </section>

    <?php include('../footer.php'); ?>