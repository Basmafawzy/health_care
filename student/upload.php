<!DOCTYPE html>
<html lang="ar" dir="rtl" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>نظام التأمين الصحي - رفع بيانات الطلاب</title>
    <!-- Include Bootstrap styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/fontawesome.css">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/owl.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet"href="../css/swiper-bundle.min.css"/>
    <script src="../js/jquery.min.js"></script>
     <script src="../js/bootstrap.min.js"></script>
    <script src="../js/isotope.min.js"></script>
    <script src="../js/owl-carousel.js"></script>
     <script src="../js/counter.js"></script>
     <script src="../js/custom.js"></script>
     <style>

body {
    background-color: #f4f4f4;
    font-family: 'Cairo', sans-serif;

}

#file {
        border: 1px solid #ced4da; 
        border-radius: 0.25rem; 
        padding: 0.375rem 0.75rem; 
    }

</style>
</head>
<body>
    <?php include('../header.php'); ?>
    <section>
    <div class="container mt-5">
    <div class="row justify-content-center align-items-center">
        <div class="col-lg-3 text-center mb-4">
            <img src="../images/upload.png" alt="Upload Icon" width="100">
        </div>

        <div class="col-lg-7">
            <h1 class="text-center mb-4">رفع بيانات الطلاب</h1>
            <form action="stdupload.php" method="post" enctype="multipart/form-data" class="needs-validation" novalidate onsubmit="return false;">
                <div class="mb-3">
                    <label for="file" class="form-label">قم بإختيار ملف الطلاب صيغة "CSV"</label>
                    <input type="file" name="file" id="file" class="form-control-file" accept=".csv" required>
                    <div class="invalid-feedback">إختار ملف </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block btn-lg mx-auto">رفع</button>
            </form>
        </div>
    </div>
    </div></div>




    </section>


    <?php include('../footer.php'); ?>
    
<script>
    document.addEventListener('DOMContentLoaded', function() {
    var forms = document.getElementsByClassName('needs-validation');

    var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            } else {
                var spinner = document.createElement('div');
                spinner.className = 'spinner-border text-primary';
                form.appendChild(spinner);

                var submitButton = form.querySelector('[type="submit"]');
                submitButton.disabled = true;


                var formData = new FormData(form);

                fetch('stdupload.php', {
                    method: 'POST',
                    body: formData
                })
                .then(response => response.json())
                .then(data => {
                    form.removeChild(spinner);

                    submitButton.disabled = false;
                
                    if (data.success) {
                        var successMessage = document.createElement('div');
                        successMessage.className = 'alert alert-success mt-3';
                        successMessage.textContent = 'تم رفع بيانات الطلاب بنجاح';
                        form.appendChild(successMessage);

                        setTimeout(function() {
                            form.removeChild(successMessage);
                        }, 5000); 
                    } else {
                        
                        console.error('Upload failed:', data.message);
                    }
                })
                .catch(error => {
                    console.error('Error during fetch:', error);
                })
                .finally(() => {
                    setTimeout(function() {
                        form.removeChild(spinner);
                    }, 2000); 
                });

                event.preventDefault();
            }
        }, false);
    });
});

</script>