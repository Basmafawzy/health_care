<!DOCTYPE html>
<html lang="ar" dir="rtl" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>نظام التأمين الصحي - إضافة طالب جديد</title>
    <!-- Include Bootstrap styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/fontawesome.css">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/owl.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet"href="../css/swiper-bundle.min.css"/>
    <script src="/Health/js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/isotope.min.js"></script>
    <script src="../js/owl-carousel.js"></script>
     <script src="../js/counter.js"></script>
     <script src="../js/custom.js"></script>
     <script src="../js/jquery-ui.js"></script>
     
     <style>

body {
    background-color: #f4f4f4;
    font-family: 'Cairo', sans-serif;

}

#file {
        border: 1px solid #ced4da; 
        border-radius: 0.25rem; 
        padding: 0.375rem 0.75rem;
    }

</style>
</head>
<body>
    <?php 
    include('../header.php');
    include '../conn.php';
    $facultyQuery = "SELECT id, name FROM faculty";
$facultyResult = mysqli_query($conn, $facultyQuery);

$levelQuery = "SELECT id, name FROM level";
$levelResult = mysqli_query($conn, $levelQuery);

$yearQuery = "SELECT id, name FROM year";
$yearResult = mysqli_query($conn, $yearQuery);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $stdname = $_POST['stdname'];
    $nationalnum = $_POST['nationalnum'];
    $faculty_id = $_POST['faculty_id'];
    $level_id = $_POST['level_id'];
    $current_cost = 0;
    $total_cost = 40000;
    $year_id = $_POST['year_id'];

    $insertQuery = "INSERT INTO student (stdname, nationalnum, faculty_id, level_id, current_cost, total_cost, year_id) 
                    VALUES ('$stdname', '$nationalnum', '$faculty_id', '$level_id', '$current_cost', '$total_cost', '$year_id')";

    $result = mysqli_query($conn, $insertQuery);

    if ($result) {
    $successMessage = "تم الإضافة بنجاح.";
    } else {
    $errorMessage = "حدث خطأ في إضافة الطالب: " . mysqli_error($conn);
    }
}
       
    ?>

    
    <div class="container mt-5">
        <h2>إضافة طالب جديد</h2>
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <div class="mb-3">
                <label for="stdname" class="form-label">إسم الطالب</label>
                <input type="text" class="form-control" id="stdname" name="stdname" required>
            </div>
            <div class="mb-3">
                <label for="nationalnum" class="form-label">الرقم القومي</label>
                <input type="text" class="form-control" id="nationalnum" name="nationalnum" required>
            </div>
            <div class="mb-3">
                <label for="faculty_id" class="form-label">الكلية</label>
                <select class="form-select" id="faculty_id" name="faculty_id" required>
                    <?php
                    while ($row = mysqli_fetch_assoc($facultyResult)) {
                        echo "<option value='{$row['id']}'>{$row['name']}</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="mb-3">
                <label for="level_id" class="form-label">الفرقة</label>
                <select class="form-select" id="level_id" name="level_id" required>
                    <?php
                    while ($row = mysqli_fetch_assoc($levelResult)) {
                        echo "<option value='{$row['id']}'>{$row['name']}</option>";
                    }
                    ?>
                </select>
            </div>
           
            <div class="mb-3">
                <label for="year_id" class="form-label">العام الأكاديمي</label>
                <select class="form-select" id="year_id" name="year_id" required>
                    <?php
                    while ($row = mysqli_fetch_assoc($yearResult)) {
                        echo "<option value='{$row['id']}'>{$row['name']}</option>";
                    }
                    ?>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">إضافة طالب</button>
        </form>

        <br />
        <?php if (!empty($successMessage)) : ?>
            <div class="alert alert-success mt-3"><?php echo $successMessage; ?></div>
        <?php endif; ?>

        <?php if (!empty($errorMessage)) : ?>
            <div class="alert alert-danger mt-3"><?php echo $errorMessage; ?></div>
        <?php endif; ?>
    </div>


    
    <?php include('../footer.php'); ?>