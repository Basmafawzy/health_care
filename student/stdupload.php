<?php
include '../conn.php';

header('Content-Type: application/json'); 

$response = ['success' => false, 'message' => 'حدث خطأ عند رفع البيانات'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_FILES['file']) && $_FILES['file']['error'] === UPLOAD_ERR_OK) {
        $file = $_FILES['file']['tmp_name'];

        $csvFile = fopen($file, 'r');

        fgetcsv($csvFile);

        while (($line = fgetcsv($csvFile)) !== FALSE) {
            $nationalnum = $conn->real_escape_string($line[0]);
            $stdname = $conn->real_escape_string($line[1]);
            $faculty_id = (int) $line[2];
            $level_id = (int) $line[3];
            $current_cost = (double) $line[4];
            $total_cost = (double) $line[5];
            $year_id = (int) $line[6];

            $query = "INSERT INTO student (nationalnum, stdname, faculty_id, level_id, current_cost, total_cost,year_id) 
                      VALUES ('$nationalnum', '$stdname', $faculty_id, $level_id, $current_cost, $total_cost,$year_id)";
            $result = $conn->query($query);

            if (!$result) {
                $response['message'] = "حدث خطأ عند رفع الملف " . $conn->error;
                echo json_encode($response);
                exit();
            }
        }

        fclose($csvFile);

        $response['success'] = true;
        $response['message'] = "تم رفع بيانات الطلاب بنجاح";
    }

    echo json_encode($response);
    exit();
}

$conn->close();
?>
