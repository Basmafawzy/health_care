
<?php

include('session.php');
?>

<style>
  .dropbtn {
  background-color: #4a8a1c;
  color: white;
  
  font-size: 16px;
  border: none;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #4a8a1c;
  color:#000000;
 
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  color:#000000;
  display: block;
}
.nav{
  padding-right:10%;
}
.dropdown-content a:hover {background-color: #4a8a1c;color:#000000; }

.dropdown:hover .dropdown-content {display: block; }

.dropdown:hover .dropbtn {background-color: #4a8a1c;}
        .main-nav {
            background-color: #4a8a1c; 
            padding: 10px;
        }

        

        .nav-link {
            color: #ffffff !important;
        }

        .nav-link:hover {
            color: #fff !important; 
        }

        .menu-trigger {
            cursor: pointer;
            color: #ffffff;
        }
        .logout {
          display: block;
    padding-left: 15px;
   padding-right: 15px;
   margin-bottom:60px;
  font-weight: 500;
  font-size: 17px;
  line-height: 40px;
  text-transform: capitalize;
  -webkit-transition: all 0.4s ease 0s;
  -moz-transition: all 0.4s ease 0s;
  -o-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
  border: transparent;
  letter-spacing: .25px;
}



    </style>
    <div class="sub-header">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-8">
          
        </div>
        <div class="col-lg-4 col-md-4">
         
        </div>
      </div>
    </div>
  </div>

  <header class="header-area header-sticky">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="main-nav">
                    <a class="navbar-brand" href="/Health">
                <img src="/Health/images/logo.png" alt="نظام التأمين الصحي" width="40">
            </a>
            <ul class="nav">
                        <li><a href="/Health/index.php" class="active dropbtn">الرئيسية</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropbtn">الهيكل</a>
                            <div class="dropdown-content">
                                <a href="/Health/category/">الجهات الصحية</a>
                                <a href="/Health/service/">الخدمات الصحية </a>
                                <a href="/Health/medicine/">الأدوية</a>
                              </div>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropbtn">الطلاب</a>
                            <div class="dropdown-content">
                                <a href="/Health/student/">إضافة طالب</a>
                                <a href="/Health/student/upload.php">رفع مجمع </a>
                              </div>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropbtn">التذاكر</a>
                            <div class="dropdown-content">
                                <a href="/Health/ticket/">إضافة تذكرة </a>
                                <a href="/Health/ticket/list_ticket.php">بحث عن تذكرة </a>
                              </div>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropbtn">الفواتير</a>
                            <div class="dropdown-content">
                                <a href="/Health/bill/">إصدار فاتورة </a>
                                <a href="/Health/bill/list_invoice.php">بحث عن فاتورة </a>
                              </div>
                        </li>
                       
                        <li class="logout-button" style="margin-right:10%;" >      <a href="/Health/logout.php">
        <button type="button" class="btn btn-danger logout">
            <i class="fas fa-user"></i> تسجيل الخروج
        </button>
    </a></li> 

                        <a class='menu-trigger'>
                            <span>القائمة</span>
                        </a>
                    </ul>
                </nav>
                
            </div>
        </div>
    </div>
  </header>
  
